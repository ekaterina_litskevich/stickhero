﻿using UnityEngine;
using System;

public class Checker : MonoBehaviour
{
    public static event Action OnTrigger;

    private bool isCollider = false;

    public void Update()
    {
        if (!isCollider)
        {
            RaycastHit2D raycastHit = Physics2D.Raycast(transform.position, Vector3.down, 0.1f);

            if (raycastHit.collider != null)
            {
                Block block = raycastHit.collider.gameObject.GetComponent<Block>();
                DoubleScore doubleScore = raycastHit.collider.gameObject.GetComponent<DoubleScore>();

                if (block != null)
                {
                    OnTrigger?.Invoke();
                    isCollider = true;
                }

                if (doubleScore != null)
                {
                    OnTrigger?.Invoke();
                    GameManager.Instance.ChangeScore();
                    isCollider = true;
                }   
            }
        }
    }
}
