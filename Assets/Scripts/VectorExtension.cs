﻿using UnityEngine;

public static class VectorExtension
{
    private const float  Unit = 2.56f;

    public static void ScaleAround(this Transform target, Transform pivot, Vector3 scale)
    {
        Transform pivotParent = pivot.parent;
        Vector3 pivotPos = pivot.position;
        pivot.parent = target;
        target.localScale = scale;
        target.position += pivotPos - pivot.position;
        pivot.parent = pivotParent;
    }


    public static Bounds GetBounds(this Transform target)
    {
        Bounds bounds = new Bounds(target.position, target.localScale * Unit);

        return bounds;
    }
}
