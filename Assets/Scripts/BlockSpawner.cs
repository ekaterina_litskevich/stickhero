﻿using DG.Tweening;
using System;
using System.Collections.Generic;
using UnityEngine;

public class BlockSpawner : MonoBehaviour
{
    private const float PersonageWidht = 0.4f;
    private const float SpeedBlock = 0.5f;
    private const float IndentForPersonage = 0.8f;
    private const float BlockHeight = 2.7f;
    private const float MaxBlockWidht = 1.0f;
    private const float OmitBlock= 0.5f;

    [SerializeField] MovingObjects movingObjects;
    [SerializeField] BlockParent blockPrefab;

    private List<BlockParent> blocks = new List<BlockParent>();

    private BlockParent blockToDestroy;

    private float halfHeightBlock;
    private float halfWidhtBlock;

    private Vector2 bounds;

    public void CreateBlockFirst(Vector2 bounds, Action<Vector2> action)
    {
        this.bounds = bounds;

        BlockParent blockParent = Instantiate(blockPrefab, movingObjects.transform);
        blockParent.Block.transform.localScale = new Vector3(MaxBlockWidht, BlockHeight);

        halfHeightBlock = blockParent.Bounds.max.y - blockParent.Bounds.center.y - OmitBlock;
        halfWidhtBlock = blockParent.Bounds.max.x - blockParent.Bounds.center.x;

        blockParent.Block.transform.position = new Vector3(-bounds.x + halfWidhtBlock, -bounds.y + halfHeightBlock);
        blocks.Add(blockParent);

        Vector2 positionPersonage = new Vector2(blockParent.Bounds.max.x - IndentForPersonage, blockParent.Bounds.max.y + 0.4f);

        action?.Invoke(positionPersonage);
    }

    public void CreateBlockSecond(Action<float, float> action)
    {
        BlockParent blockParent = Instantiate(blockPrefab, transform);

        float randomWight = UnityEngine.Random.Range(PersonageWidht, MaxBlockWidht);
        blockParent.Block.transform.localScale = new Vector2(randomWight, BlockHeight);
        blockParent.Block.transform.position = new Vector2(bounds.x + halfWidhtBlock, -bounds.y + halfHeightBlock);

        blocks.Add(blockParent);

        blockParent.SetDoubleScore();

        float blockFirstWidht = blocks[0].Bounds.size.x;
        float blockSecondWidht = blocks[1].Bounds.size.x;
        float maxDistance = bounds.x - (-bounds.x + blockFirstWidht + (blockSecondWidht / 2));
        float minDistance = bounds.x - blockSecondWidht;

        float randomDistance = UnityEngine.Random.Range(minDistance, maxDistance);

        blockParent.transform.DOMoveX(blockParent.transform.position.x - randomDistance, SpeedBlock).OnComplete(() =>
        {
            blockParent.transform.SetParent(movingObjects.transform);

            float distanceForParentObjects = blocks[1].Bounds.min.x - blocks[0].Bounds.min.x;
            float distanceForPersonage = blocks[1].Bounds.max.x - blocks[0].Bounds.max.x;

            RemoveBlockCollider();

            action?.Invoke(distanceForParentObjects, distanceForPersonage);
        });
    }

    public void RemoveAtList()
    {
        blockToDestroy = blocks[0];
        blocks.RemoveAt(0);
    }

    public void DestroyBlock()
    {
        Destroy(blockToDestroy.gameObject);
    }

    public void DestroyAllBlocks()
    {
        for (int i = blocks.Count; i > 0; i--)
        {
            Destroy(blocks[i - 1].gameObject);
            blocks.RemoveAt(i - 1);
        }
    }
    
    private void RemoveBlockCollider()
    {
        blocks[0].Block.BoxCollider2D.enabled = false;
    }
}
