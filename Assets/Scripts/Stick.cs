﻿using UnityEngine;

public class Stick : MonoBehaviour
{
    [SerializeField] BoxCollider2D boxCollider;
    public Bounds Bounds => boxCollider.bounds;
}
