﻿using UnityEngine;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviourSingleton<GameManager>
{
    [SerializeField] private Camera cam = null;
    [SerializeField] private BlockSpawner blockSpawner = null;
    [SerializeField] private Personage personagePrefab = null;
    [SerializeField] private MovingObjects movingObjects = null;
    [SerializeField] private UIManagerOnGameScene canvas = null;

    private Personage personage = null;
    private StickParent stickParent = null;

    private int score = 0;

    public GameState gameState;

    private Vector3 bounds;

    protected override void SingletonStarted()
    {
        gameState = GameState.Start;

        cam.orthographicSize = Screen.height / 200.0f;

        CreateBlocksFirst();
    }
    public void ChangeScene()
    {
        SceneManager.LoadScene("HomeScreen");
        DestroyInstance();
    }

    public void ChangeScore()
    {
        SoundManager.Instance.PlaySFX(SFX.DoubleScore);

        score++;

        canvas.DoubleScore(score);
    }

    public void Restart()
    {
        gameState = GameState.Start;

        Destroy(personage.gameObject);
        personage = null;

        Destroy(stickParent.gameObject);
        stickParent = null;

        blockSpawner.DestroyAllBlocks();

        movingObjects.transform.position = Vector2.zero;

        score = 0;

        canvas.Restart(score);

        CreateBlocksFirst();
    }

    public void OnMoveEnd()
    {
        blockSpawner.DestroyBlock();
        Destroy(stickParent.gameObject);
        stickParent = null;
        gameState = GameState.CreateStick;
    }

    public void Losing()
    {
        gameState = GameState.Losing;

        SoundManager.Instance.PlaySFX(SFX.PersonageDrop);
        cam.DOShakePosition(1.0f, new Vector2(0.0f, 0.05f), 10, 45.0f).OnComplete(() =>
        {
            int record = PlayerPrefs.GetInt("record", 0);

            if (score > record)
            {
                record = score;

                PlayerPrefs.SetInt("record", record);
                PlayerPrefs.Save();
                canvas.NewRecord();
            }
            canvas.Losing(score, record);
        });
    }

    private void CreateBlocksFirst()
    {
        bounds = new Vector3(Screen.width, Screen.height);
        bounds = cam.ScreenToWorldPoint(bounds);

        blockSpawner.CreateBlockFirst(bounds, CreatePersonage);
        blockSpawner.CreateBlockSecond(BlockSpawner_OnCreateBlockSecond);
    }

    private void CreatePersonage(Vector2 position)
    {
        personage = Instantiate(personagePrefab, movingObjects.transform);
        personage.transform.position = position;
    }

    private void BlockSpawner_OnCreateBlockSecond(float distanceForParentObjects, float distanceForPersonage)
    {
        gameState = GameState.CreateStick;
        personage.NextPosition(distanceForPersonage);
        movingObjects.NextPosition(distanceForParentObjects);
    }

    private void Stick_OnStickFell(Vector2 edgeRightStick)
    {
        if (gameState != GameState.MovePersonage)
        {
            gameState = GameState.Losing;

            float distance = Vector2.Distance(personage.transform.position, edgeRightStick);
            personage.NextPosition(distance);
        }

        personage.Move(bounds);
    }

    public void Personage_OnMotionEnd()
    {
        gameState = GameState.MoveObjects;

        SoundManager.Instance.PlaySFX(SFX.LevelUp);

        score++;
        canvas.ChangeScore(score);
        movingObjects.MoveObjects(OnMoveEnd);
        blockSpawner.RemoveAtList();
        blockSpawner.CreateBlockSecond(BlockSpawner_OnCreateBlockSecond);
    }

    public void StickDrop()
    {
        stickParent.transform.DORotate(Vector3.back * 180, 0.7f);
    }

    private void Update()
    {
        if (gameState == GameState.CreateStick)
        {
            if (Input.GetMouseButtonDown(0))
            {
                stickParent = Instantiate(Resources.Load<StickParent>("StickParent"), movingObjects.transform);
                stickParent.transform.position = new Vector2(personage.transform.position.x + 0.5f, personage.transform.position.y - 0.4f);
            }
            else if (Input.GetMouseButton(0))
            {
                if (stickParent != null)
                {
                    stickParent.ScaleStick();
                    SoundManager.Instance.PlaySFX(SFX.StickGrowth);
                }
            }
            else if (Input.GetMouseButtonUp(0))
            {
                if (stickParent != null)
                {
                    gameState = GameState.DropStick;
                    stickParent.SetCheker();
                    stickParent.transform.DORotate(Vector3.back * 90, 0.7f).OnComplete(() =>
                    {
                        SoundManager.Instance.PlaySFX(SFX.StickDrop);
                        Vector2 edgeRight = stickParent.Bounds.max;
                        Stick_OnStickFell(edgeRight);
                    });
                }
            }
        }
    }
}
