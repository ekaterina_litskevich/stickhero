﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SoundSource", menuName ="SoundManager")]
public class SoundSource : ScriptableObject
{
    [SerializeField] List<AudioClip> audioClips = new List<AudioClip>();

    public AudioClip GetSound(string soundName)
    {
        AudioClip audioClip = null;

        foreach (var clip in audioClips)
        {
            if (clip.name == soundName)
            {
                audioClip = clip;
                break;
            }
        }

        return audioClip;
    }
}
