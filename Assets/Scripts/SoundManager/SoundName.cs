﻿public static class BGM
{
    public const string Main = "Main";
}


public static class SFX
{
    public const string StickGrowth = "StickGrowth";
    public const string StickDrop = "StickDrop";
    public const string LevelUp = "LevelUp";
    public const string DoubleScore = "DoubleScore";
    public const string PersonageDrop = "PersonageDrop";
}
