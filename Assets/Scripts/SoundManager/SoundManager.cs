﻿using UnityEngine;

public class SoundManager : MonoBehaviourSingleton<SoundManager>
{
    const string MuteKey = "Mute";

    [SerializeField] AudioSource audioSourceSFX = null;
    [SerializeField] AudioSource audioSourceBGM = null;

    [SerializeField] SoundSource soundSourceSFX = null;
    [SerializeField] SoundSource soundSourceBGM = null;

    public bool IsMute { get; private set; }

    protected override void SingletonAwakened()
    {
        IsMute = PlayerPrefsExtension.GetBool(MuteKey);

        if (IsMute)
            Mute();
        else
            UnMute();
    }

    public void PlaySFX(string soundName)
    {
        AudioClip audioClip = soundSourceSFX.GetSound(soundName);
        if (audioClip != null)
        {
            audioSourceSFX.PlayOneShot(audioClip);
        }
    }

    public void PlayBGM(string soundName)
    {
        AudioClip audioClip = soundSourceBGM.GetSound(soundName);
        if (audioClip != null)
        {
            audioSourceBGM.clip = audioClip;
            audioSourceBGM.loop = true;
            audioSourceBGM.Play();
        }
    }

    public void Mute()
    {
        PlayerPrefsExtension.SetBool(MuteKey, true);
        audioSourceSFX.mute = true;
        audioSourceBGM.mute = true;
    }

    public void UnMute()
    {
        PlayerPrefsExtension.SetBool(MuteKey, false);
        audioSourceSFX.mute = false;
        audioSourceBGM.mute = false;
    }
}
