﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoubleScore : MonoBehaviour
{
    [SerializeField] BoxCollider2D doubleScoreCollider;

    public BoxCollider2D DoubleScoreCollider => doubleScoreCollider;
}
