﻿using UnityEngine.UI;
using UnityEngine;
using DG.Tweening;

public class UIManagerOnGameScene : MonoBehaviour
{
    [SerializeField] private Image whiteScreenImage;
    [SerializeField] private Image scoreImage;
    [SerializeField] private Button homeSreenButton;
    [SerializeField] private Button restartButton;
    [SerializeField] private Text scoreText;
    [SerializeField] private Text scoreEndText;
    [SerializeField] private Text recordText;
    [SerializeField] private Text addScoreText;
    [SerializeField] private Text newRecordText;

    private void OnEnable()
    {
        restartButton.onClick.AddListener(RestartButton_OnClick);
        homeSreenButton.onClick.AddListener(HomeSreenButton_OnClick);
    }

    private void OnDisable()
    {
        restartButton.onClick.RemoveListener(RestartButton_OnClick);
        homeSreenButton.onClick.RemoveListener(HomeSreenButton_OnClick);
    }

    private void RestartButton_OnClick()
    {
        GameManager.Instance.Restart();
    }

    private void HomeSreenButton_OnClick()
    {
        GameManager.Instance.ChangeScene();
    }

    public void DoubleScore(int score)
    {
        Sequence sequence = DOTween.Sequence();

        addScoreText.gameObject.SetActive(true);

        addScoreText.color = new Color(addScoreText.color.r, addScoreText.color.g, addScoreText.color.b, 0.0f);
        sequence
            .Append(addScoreText.DOFade(1.0f, 1.0f))
            .Append(addScoreText.DOFade(0.0f, 1.0f));

        ChangeScore(score);
    }

    public void Restart(int score)
    {
        whiteScreenImage.gameObject.SetActive(false);
        newRecordText.gameObject.SetActive(false);
        scoreImage.gameObject.SetActive(true);
        scoreText.text = string.Format("{0}", score);
    }

    public void ChangeScore(int score)
    {
        newRecordText.gameObject.SetActive(false);
        scoreText.text = string.Format("{0}", score);
    }

    public void Losing(int score, int record)
    {
        scoreImage.gameObject.SetActive(false);
        whiteScreenImage.gameObject.SetActive(true);

        scoreEndText.text = string.Format("{0}", score);
        recordText.text = string.Format("{0}", record);
    }

    public void NewRecord()
    {
        newRecordText.gameObject.SetActive(true);
    }
}
