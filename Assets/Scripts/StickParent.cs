﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class StickParent : MonoBehaviour
{
    public static event Action OnTriggerChecker;

    [SerializeField] private Transform pivotToScale;
    [SerializeField] private Stick stick;
    [SerializeField] private Checker cheker;

    private bool isBlock = false;

    public Bounds Bounds
    {
        get
        {
            return stick.Bounds;
        }
    }

    private void OnEnable()
    {
        Checker.OnTrigger += CallEvent_OnTrigger;
    }

    private void OnDisable()
    {
        Checker.OnTrigger -= CallEvent_OnTrigger;
    }

    public void SetCheker()
    {
        cheker.transform.localPosition = new Vector3(cheker.transform.localPosition.x, stick.transform.localPosition.y * 2 - 0.04f);
        cheker.gameObject.SetActive(true);
    }

    public void ScaleStick()
    {
        stick.transform.ScaleAround(pivotToScale, new Vector3(stick.transform.localScale.x, stick.transform.localScale.y + 0.03f));
    }

    private void CallEvent_OnTrigger()
    {
        GameManager.Instance.gameState = GameState.MovePersonage;
    }
}
