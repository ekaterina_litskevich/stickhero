﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockParent : MonoBehaviour
{
    [SerializeField] Block block;
    [SerializeField] DoubleScore doubleScore;

    public Block Block => block;
    public DoubleScore DoubleScore => doubleScore;
    public Bounds Bounds => block.transform.GetBounds();
 
    public void SetDoubleScore()
    {
        doubleScore.transform.localPosition = new Vector3(Bounds.center.x, Bounds.max.y - 0.05f);
        doubleScore.gameObject.SetActive(true);
    }
}
