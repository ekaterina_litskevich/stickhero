﻿using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIManagerOnScreenHome : MonoBehaviour
{
    [SerializeField] private Text gameNameText;
    [SerializeField] private Button playButton;
    [SerializeField] private Button onMusicButton;
    [SerializeField] private Button offMusicButton;
    void Start()
    {
        SoundManager.Instance.PlayBGM(BGM.Main);

        if (SoundManager.Instance.IsMute)
        {
            onMusicButton.gameObject.SetActive(false);
            offMusicButton.gameObject.SetActive(true);
        }

        playButton.onClick.AddListener(ChangeScene);
        onMusicButton.onClick.AddListener(OffMusic);
        offMusicButton.onClick.AddListener(OnMusic);
    }

    private void OffMusic()
    {
        SoundManager.Instance.Mute();

        onMusicButton.gameObject.SetActive(false);
        offMusicButton.gameObject.SetActive(true);
    }

    private void OnMusic()
    {
        SoundManager.Instance.UnMute();

        offMusicButton.gameObject.SetActive(false);
        onMusicButton.gameObject.SetActive(true);
    }

    private void ChangeScene()
    {
        playButton.gameObject.SetActive(false);
        onMusicButton.gameObject.SetActive(false);
        offMusicButton.gameObject.SetActive(false);

        SceneManager.LoadScene("Game");
    }
}
