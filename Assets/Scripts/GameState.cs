﻿public enum GameState
{
     None = 0,
     Start = 1,
     CreateStick = 2,
     DropStick = 3,
     MovePersonage = 4,
     MoveObjects = 5,
     Losing = 6
}
