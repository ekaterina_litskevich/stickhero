﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour
{
    [SerializeField] BoxCollider2D blockCollider;
    
    public BoxCollider2D BoxCollider2D => blockCollider;
}
