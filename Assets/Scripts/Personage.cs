﻿using UnityEngine;
using DG.Tweening;

public class Personage : MonoBehaviour
{
    private const int Speed = 5; 

    private float distance;

    public void NextPosition(float distance)
    {
        this.distance = distance;
    }

    public void Move(Vector3 bounds)
    {
        float motionTime = distance / Speed;

        if (GameManager.Instance.gameState == GameState.MovePersonage)
        {
            transform.DOMoveX(transform.position.x + distance, motionTime).OnComplete(()=>
            {
                GameManager.Instance.Personage_OnMotionEnd();
            });
        }
        else
        {
            transform.DOMoveX(transform.position.x + distance, motionTime).OnComplete(()=>
            {
                GameManager.Instance.StickDrop();

                transform.DOMoveY(-bounds.y - 2, 0.3f).OnComplete(()=>
                {
                    GameManager.Instance.Losing();
                });
            });
        }
    }
}
