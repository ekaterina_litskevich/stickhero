﻿using UnityEngine;
using DG.Tweening;
using System;

public class MovingObjects : MonoBehaviour
{
    private const float Speed = 0.5f;

    private float distance;

    public void NextPosition(float distance)
    {
        this.distance = distance;
    }
    public void MoveObjects(Action action)
    {
        transform.DOMoveX(transform.position.x - distance, Speed).OnComplete(()=> 
        {
            action?.Invoke();
        });
    }
}
