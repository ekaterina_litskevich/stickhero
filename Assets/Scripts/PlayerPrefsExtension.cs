﻿using UnityEngine;
public static class PlayerPrefsExtension
{
    public static bool GetBool(string key, bool defaultValue = false)
    {
        bool value = false;

        int defaultValueInt = defaultValue ? 1 : 0;
        int valueInt = PlayerPrefs.GetInt(key, defaultValueInt);

        if (valueInt == 1)
            value = true;

        return value;
    }


    public static void SetBool(string key, bool value)
    {
        int valueInt = value ? 1 : 0;
        PlayerPrefs.SetInt(key, valueInt);
    }
}
